﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LogicaDifusa
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        double valor;

        private void button1_Click(object sender, EventArgs e)
        {
            valor = double.Parse(textBox1.Text);
            if (LogicaDifusa.FuncionBooleana(valor, 200) == 1)
            {
                label1.Text = "Hay electricidad";
            }
            else
            {
                label1.Text = "Se cortó la luz";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            valor = double.Parse(textBox1.Text);
            if (LogicaDifusa.FuncionBooleanaInversa(valor, 200) == 0)
            {
                label1.Text = "Hay electricidad";
            }
            else
            {
                label1.Text = "Se cortó la luz";
            }
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            double resultado;
            if (checkBox1.Checked)
            {
                resultado = LogicaDifusa.FuncionGrado(vScrollBar1.Value, 180, 200);
                if (resultado == 0)
                {
                    label2.Text = "No hay electricidad";
                }
                else if (resultado == 1)
                {
                    label2.Text = "Todo bien";

                }
                else
                {
                    label2.Text = "Baja tensión";
                }
            }
            else
            {
                resultado = LogicaDifusa.FuncionGradoInversa(vScrollBar1.Value, 180, 200);
                if (resultado == 1)
                {
                    label2.Text = "No hay electricidad";
                }
                else if (resultado == 0)
                {
                    label2.Text = "Todo bien";

                }
                else
                {
                    label2.Text = "Baja tensión";
                }
            }
      


        }

        int grado = 1;
        private void timer1_Tick(object sender, EventArgs e)
        {
            double valor = hScrollBar1.Value;
            double resultado = LogicaDifusa.FuncionTriangulo(valor, 25, 50, 75);

            if (resultado == 1)
            {
                label3.Text = "FUERTE";
            }
            if (resultado == 0)
            {
                label3.Text = " SIN FUERZA";
            }
            else
            {
                label3.Text = resultado.ToString() + "  % de fuerza";
            }

            hScrollBar1.Value += grado;
            if (hScrollBar1.Value >= hScrollBar1.Maximum  || hScrollBar1.Value <=0 )
            {
                grado *= -1;
            }
            


        }
    }
}
