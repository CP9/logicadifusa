﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogicaDifusa
{
    public class LogicaDifusa
    {
        public static double FuncionBooleana(double valor, double x0)
        {
            double membresia = (valor < x0) ? 0.0f : 1.0f;

            return membresia;
        }

        public static double FuncionBooleanaInversa(double valor, double x0)
        {
            double membresia = (valor < x0) ? 1.0f : 0.0f;

            return membresia;
        }

        public static double FuncionGrado(double valor, double x0, double x1)
        {
            double membresia;

            if (valor < x0)
            {
                membresia = 0;
            }
            else if (valor >= x0 && valor < x1)
            {
                membresia = (valor / (x1 - x0)) - (x0 / (x1 - x0));
            }
            else
            {
                membresia = 1;
            }
            return membresia;
        }

        public static double FuncionGradoInversa(double valor, double x0, double x1)
        {
            double membresia;

            if (valor < x0)
            {
                membresia = 1;
            }
            else if (valor >= x0 && valor < x1)
            {
                membresia = -((valor / (x1 - x0)) + (x0 / (x1 - x0)));
            }
            else
            {
                membresia = 0;
            }
            return membresia;
        }

        public static double FuncionTriangulo(double valor, double x0, double x1, double x2)
        {
            double membresia =0;

            if (valor < x0 || valor >= x2)
            {
                membresia = 0;
            }
            else if (valor >= x0 && valor < x1)
            {
                membresia = (valor / (x1 - x0)) - (x0 / (x1 - x0));
            }
            else if (valor >= x1 && valor < x2)
            {
                membresia = -((valor / (x2 - x1)) + (x2 / (x2 - x1)));
            }
            
            return membresia;

        }

        public static double FuncionTrapezoide(double valor, double x0, double x1, double x2, double x3)
        {
            double membresia = 0;

            if (valor < x0 || valor >= x3)
            {
                membresia = 0;
            }
            else if (valor >= x0 && valor < x1)
            {
                membresia = (valor / (x1 - x0)) - (x0 / (x1 - x0));
            }
            else if(valor >= x1 && valor <= x2)
            {
                membresia = 1;
            }
            else if (valor > x2 && valor < x3)
            {
                membresia = -((valor / (x3 - x2)) + (x3 / (x3 - x2)));
            }

            return membresia;

        }

        public static double AND (double a, double b)
        {
            double valor = Math.Min(a, b);
            return valor;
        }

        public static double OR(double a, double b)
        {
            return Math.Max(a, b);
        }

        public static double NOT(double valor)
        {
            return (1 - valor);
        }
    }
}
